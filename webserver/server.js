const express = require('express');
const PORT = process.env.PORT || 443;
const app = express();
const https = require('https');
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');

const cert_file = path.join(__dirname, "../secrets/mutate.pem");
const key_file = path.join(__dirname, "../secrets/mutate.key");

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

// New https code
https.createServer({
  key: fs.readFileSync(key_file),
  cert: fs.readFileSync(cert_file)
}, app).listen(PORT, (req,res)=> {
    console.log('server',`Webserver listening on ${PORT}`);
});

// Original http code
// app.listen(PORT, (req,res)=> {
//     logger.log('server',`Webserver listening on ${PORT}`);
// });

// body parsing middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use((req, res, next) => {
//     setCORSHeaders(res);
//     next();
// })

// app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
    res.send('Looks good');
})

app.all('/mutate', (req, res) => {
  // console.log(JSON.stringify(req.body, null, 2));
  let kind = req.body.request.object.kind,
      obj_name = req.body.request.object.metadata.generateName;
  console.log(`Receiving request -- Kind: ${kind}, Name: ${obj_name}`);
  let body = req.body;
  body.response = mutate(req.body.request);
  res.send(body);
});


/////////////////////////////////////

// Input is req.body.request
function mutate(request){
  let response = {}, patch_list = [], patch_base64,
  containers = request.object.spec.containers;

  // initializing
  response.uid = request.uid;
  response.allowed = true;
  response.patchType = "JSONPatch";

  for(let i = 0; i < containers.length; i++){
    patch_list.push(createPatch(containers[i].env, i));
  }

  console.log("Patch List: " + JSON.stringify(patch_list, null, 2)); // debug

  // Base64 encde the patches
  patch_base64 = Buffer.from(JSON.stringify(patch_list)).toString("base64");
  console.log("patch base64: " + patch_base64);
  response.patch = patch_base64;

  return response;
}

// Creating patch for a single container
// request.object.spec.containers[container_idx].env
// This function returns patch for this single container
function createPatch(env=[], container_idx){
  let proxy = "127.0.0.1:8000",
      no_proxy = "*.nasdaq.com,*ften.com",
      var_list = ['http_proxy', "HTTP_PROXY", "FTP_PROXY", "no_proxy"],
      env_vars_to_add;


  env_vars_to_add = [
    {name: "http_proxy", value: "http://165.225.56.24:9400"},
    {name: "HTTP_PROXY", value: "http://165.225.56.24:9400"},
    {name: "https_proxy", value: "http://165.225.56.24:9400"},
    {name: "HTTPS_PROXY", value: "http://165.225.56.24:9400"},
    {name: "FTP_PROXY", value: "http://165.225.56.24:9400"},
    {name: "ftp_proxy", value: "http://165.225.56.24:9400"},
    {name: "no_proxy", value: "docker,localhost,127.0.0.1,local,local,localaddress,localdomain.com,localdomain.com,nasdaq.com,nasdaq.com,nasdaqomx.com,nasdaqomx.com,*ften.com,ften.com,192.168.0.0/16,10.0.0.0/8,172.16.0.0/12"},
    {name: "NO_PROXY", value: "docker,localhost,127.0.0.1,local,local,localaddress,localdomain.com,localdomain.com,nasdaq.com,nasdaq.com,nasdaqomx.com,nasdaqomx.com,*ften.com,ften.com,192.168.0.0/16,10.0.0.0/8,172.16.0.0/12"},
  ];

  if(env.length > 0){
    // If there are env var defined already

    // Add all non-proxy related vars to env_vars_to_add array
    // Then replace all env vars with env_vars_to_add.
    // This way we simplify the following scenarios
    //  1. Proxy env vars are defined but with wrong value. We would need to replace values for those vars.
    //  2. Only some proxy env vars are defined. We would need to replace values for the defined vars, and add the missing ones.
    env.forEach(env_var_obj => {
      // If this env_var's name isn't in the var_list, then we need to put it back.
      if(!var_list.includes(env_var_obj.name)) env_vars_to_add.push(env_var_obj);
    });
    return {"op": "replace", "path": `/spec/containers/${container_idx}/env`, "value": env_vars_to_add};
  }
  else {
    return {"op": "add", "path": `/spec/containers/${container_idx}/env`, "value": env_vars_to_add};
  }
}

///////////////////
// Tests
app.all('/test_env', (req, res) => {
  req.body = request_env;

  let body = req.body;
  body.response = mutate(req.body.request);
  res.send(body);
});
app.all('/test_noenv', (req, res) => {
  req.body = request_noenv;

  let body = req.body;
  body.response = mutate(req.body.request);
  res.send(body);
});

let request_noenv = {
  "kind": "AdmissionReview",
  "apiVersion": "admission.k8s.io/v1beta1",
  "request": {
    "uid": "a6ed72be-51b2-4511-bd99-98bfb82ef112",
    "kind": {
      "group": "",
      "version": "v1",
      "kind": "Pod"
    },
    "resource": {
      "group": "",
      "version": "v1",
      "resource": "pods"
    },
    "requestKind": {
      "group": "",
      "version": "v1",
      "kind": "Pod"
    },
    "requestResource": {
      "group": "",
      "version": "v1",
      "resource": "pods"
    },
    "namespace": "default",
    "operation": "CREATE",
    "userInfo": {
      "username": "system:serviceaccount:kube-system:replicaset-controller",
      "uid": "92155d37-fcee-4e6f-8cd3-7c60a31e0523",
      "groups": [
        "system:serviceaccounts",
        "system:serviceaccounts:kube-system",
        "system:authenticated"
      ]
    },
    "object": {
      "kind": "Pod",
      "apiVersion": "v1",
      "metadata": {
        "generateName": "nginx-554b9c67f9-",
        "creationTimestamp": null,
        "labels": {
          "app": "nginx",
          "pod-template-hash": "554b9c67f9"
        },
        "ownerReferences": [
          {
            "apiVersion": "apps/v1",
            "kind": "ReplicaSet",
            "name": "nginx-554b9c67f9",
            "uid": "c20784b4-7dc0-46da-8c12-bd3040e9e061",
            "controller": true,
            "blockOwnerDeletion": true
          }
        ]
      },
      "spec": {
        "volumes": [
          {
            "name": "default-token-mr4vb",
            "secret": {
              "secretName": "default-token-mr4vb"
            }
          }
        ],
        "containers": [
          {
            "name": "nginx",
            "image": "nginx",
            "resources": {},
            "volumeMounts": [
              {
                "name": "default-token-mr4vb",
                "readOnly": true,
                "mountPath": "/var/run/secrets/kubernetes.io/serviceaccount"
              }
            ],
            "terminationMessagePath": "/dev/termination-log",
            "terminationMessagePolicy": "File",
            "imagePullPolicy": "Always"
          }
        ],
        "restartPolicy": "Always",
        "terminationGracePeriodSeconds": 30,
        "dnsPolicy": "ClusterFirst",
        "serviceAccountName": "default",
        "serviceAccount": "default",
        "securityContext": {},
        "schedulerName": "default-scheduler",
        "tolerations": [
          {
            "key": "node.kubernetes.io/not-ready",
            "operator": "Exists",
            "effect": "NoExecute",
            "tolerationSeconds": 300
          },
          {
            "key": "node.kubernetes.io/unreachable",
            "operator": "Exists",
            "effect": "NoExecute",
            "tolerationSeconds": 300
          }
        ],
        "priority": 0,
        "enableServiceLinks": true
      },
      "status": {}
    },
    "oldObject": null,
    "dryRun": false,
    "options": {
      "kind": "CreateOptions",
      "apiVersion": "meta.k8s.io/v1"
    }
  }
};

let request_env = {
  "kind": "AdmissionReview",
  "apiVersion": "admission.k8s.io/v1beta1",
  "request": {
    "uid": "070cfbed-1075-4299-aeae-9ac4a236b3cf",
    "kind": {
      "group": "",
      "version": "v1",
      "kind": "Pod"
    },
    "resource": {
      "group": "",
      "version": "v1",
      "resource": "pods"
    },
    "requestKind": {
      "group": "",
      "version": "v1",
      "kind": "Pod"
    },
    "requestResource": {
      "group": "",
      "version": "v1",
      "resource": "pods"
    },
    "namespace": "default",
    "operation": "CREATE",
    "userInfo": {
      "username": "system:serviceaccount:kube-system:replicaset-controller",
      "uid": "92155d37-fcee-4e6f-8cd3-7c60a31e0523",
      "groups": [
        "system:serviceaccounts",
        "system:serviceaccounts:kube-system",
        "system:authenticated"
      ]
    },
    "object": {
      "kind": "Pod",
      "apiVersion": "v1",
      "metadata": {
        "generateName": "nginx-677cfcff9d-",
        "creationTimestamp": null,
        "labels": {
          "app": "nginx",
          "pod-template-hash": "677cfcff9d"
        },
        "ownerReferences": [
          {
            "apiVersion": "apps/v1",
            "kind": "ReplicaSet",
            "name": "nginx-677cfcff9d",
            "uid": "6b2ac0e7-6f3f-4bfb-a584-c70e626dd620",
            "controller": true,
            "blockOwnerDeletion": true
          }
        ]
      },
      "spec": {
        "volumes": [
          {
            "name": "default-token-mr4vb",
            "secret": {
              "secretName": "default-token-mr4vb"
            }
          }
        ],
        "containers": [
          {
            "name": "nginx",
            "image": "nginx",
            "env": [
              {
                "name": "demo_env_var",
                "value": "Hello from the environment"
              }
            ],
            "resources": {},
            "volumeMounts": [
              {
                "name": "default-token-mr4vb",
                "readOnly": true,
                "mountPath": "/var/run/secrets/kubernetes.io/serviceaccount"
              }
            ],
            "terminationMessagePath": "/dev/termination-log",
            "terminationMessagePolicy": "File",
            "imagePullPolicy": "Always"
          }
        ],
        "restartPolicy": "Always",
        "terminationGracePeriodSeconds": 30,
        "dnsPolicy": "ClusterFirst",
        "serviceAccountName": "default",
        "serviceAccount": "default",
        "securityContext": {},
        "schedulerName": "default-scheduler",
        "tolerations": [
          {
            "key": "node.kubernetes.io/not-ready",
            "operator": "Exists",
            "effect": "NoExecute",
            "tolerationSeconds": 300
          },
          {
            "key": "node.kubernetes.io/unreachable",
            "operator": "Exists",
            "effect": "NoExecute",
            "tolerationSeconds": 300
          }
        ],
        "priority": 0,
        "enableServiceLinks": true
      },
      "status": {}
    },
    "oldObject": null,
    "dryRun": false,
    "options": {
      "kind": "CreateOptions",
      "apiVersion": "meta.k8s.io/v1"
    }
  }
}
