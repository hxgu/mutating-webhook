FROM node:10.8.0-alpine
WORKDIR /apps/mutate

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json ./

RUN npm config set strict-ssl false && npm install

COPY webserver ./webserver

EXPOSE 443

# ENTRYPOINT ["npm", "start"]

